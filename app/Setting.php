<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'value'];

    /**
     * Get setting for the given name.
     *
     * @param string $name
     * @return string
     */
    public static function get(string $name): string
    {
        return (new static)->firstOrCreate(['name' => $name])->value;
    }

    /**
     * Set the given settings.
     *
     * @param array $settings
     * @return void
     */
    public static function set(array $settings): void
    {
        foreach ($settings as $name => $value) {
            (new static)->firstOrCreate(['name' => $name])->update(['value' => $value]);
        }
    }
}
