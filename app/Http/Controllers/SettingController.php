<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amount = (int) Setting::get('payment_amount') / 100;

        return view('settings', compact('amount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->validate([
            'payment_amount' => 'required|numeric',
        ]);

        $data['payment_amount'] *= 100;

        Setting::set($data);

        return back()->withSuccess('Setting updated successfully.');
    }
}
