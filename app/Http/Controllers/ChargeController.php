<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Transaction;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class ChargeController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $amount = Setting::get('payment_amount');

        return view('charge', compact('amount'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $amount = Setting::get('payment_amount');

        $details = Stripe::charges()->create([
            'amount' => $amount,
            'currency' => 'usd',
            'source' => $request->get('stripeToken'),
        ]);

        Transaction::create(compact('details'));

        return back()->withSuccess("Paid successfully.");
    }
}
