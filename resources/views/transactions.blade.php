@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Charge</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <div class="row">
                            @foreach ($transactions as $transition)
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">${{ $transition->details['amount'] / 100 }}</h5>
                                            <p class="card-text">{{ $transition->details['source']['name'] }}</p>
                                            <p href="#" class="card-text text-success">{{ $transition->details['status'] }}</p>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="https://js.stripe.com/v3/"></script>
