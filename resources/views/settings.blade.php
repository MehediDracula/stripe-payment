@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Settings</div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('settings.update') }}">
                        @csrf
                        @method('put')

                        <div class="form-group row">
                            <label for="payment-amount" class="col-sm-3 col-form-label">Payment Amount</label>

                            <div class="col-sm-9">
                                <input
                                    type="number"
                                    name="payment_amount"
                                    class="form-control {{ $errors->has('payment_amount') ? 'is-invalid': '' }}"
                                    id="payment-amount"
                                    placeholder="Payment Amount"
                                    value="{{ old('payment_amount', $amount) }}">

                                <div class="invalid-feedback">{{ $errors->first('payment_amount') }}</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
