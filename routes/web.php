<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'transactions');

Auth::routes();

Route::get('/charge', 'ChargeController@create')->name('charge')->middleware('auth');
Route::put('/charge', 'ChargeController@store')->name('charge.store')->middleware('auth');

Route::get('/transactions', 'TransactionController@index')->name('transactions')->middleware('auth');

Route::get('/settings', 'SettingController@index')->name('settings')->middleware('auth');
Route::put('/settings', 'SettingController@update')->name('settings.update')->middleware('auth');
